public class Sorting {
	
	public void quicksort(int[] input, int first,int last){
		if (first < last){
				int q = partition(input, first, last);
				quicksort(input, first, q-1);
				quicksort(input,q+1,last);
		}
	}
	
	public int partition(int[] input, int first, int last){
		
		int r = input[last];
		int i = first - 1;
		int j = first;
		int temp;
		for(;j<last;j++){
			if (input[j] <= r){
				temp = input[j];
				input[j] = input[i+1];
				input[i+1] = temp;
				i++;
			}
		}
		input[last] = input[i+1];
		input[i+1] = r;
		return i+1;
	}
	
	public int[] mergeSort(int[] a, int first, int last){
		//System.out.println("first ="+first);
		//System.out.println("last="+last);
		if((last-first == 0))
			return a;
		int mid = (first + last + 1)/2;
		mergeSort(a, first,mid -1);
		mergeSort(a,mid,last);
		return merge(a,first,mid-1,last);
	}
	public int[] merge(int[] a, int first, int middle, int last){
		int right = middle+1;
		int left = first;
		int[] temp = new int[last - first + 1];
		int i = 0;
		while (left <= middle && right <= last){
			if (a[left] < a[right]){
				temp[i++] = a[left++];
			}else{
				temp[i++] = a[right++];
			}
		}
		while(left <= middle){
			temp[i++] = a[left++];
		}
		while(right <= last){
			temp[i++] = a[right++];
		}
		i= 0;
		while(first <= last)
			a[first++] = temp[i++];
		return a;
	}

}
