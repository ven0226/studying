public class LinkedList<T> {
    Node head = null;
    public void create(T val){
        Node<T> newNode = new Node<T>();
        newNode.setData(val);
        newNode.setNext(null);
        if(head == null){
            head = newNode;
        }else{
            Node current = head;
            while(current.getNext() != null){
                current = current.getNext();
            }
            current.setNext(newNode);
        }
    }

    public void read(){
        Node current = head;
        while(current != null){
            System.out.println("Data = " + current.getData());
            current = current.getNext();
        }
    }

    /*Updates the first occurrence of oldValue*/
    public boolean update(T oldValue, T newValue){
        Node current = head;
        boolean updated = false;
        while (current != null){
            if(current.getData() == oldValue){
                current.setData(newValue);
                updated = true;
                break;
            }else{
                current = current.getNext();
            }
        }
        return updated;
    }

    /* Deletes the first occurrence of value */
    public boolean delete(T value){
        Node prev = null;
        Node current = head;
        boolean deleted = false;
        while(current != null){
            if(current.getData() == value){
                if (current == head){
                    head = current.getNext();
                }else{
                    prev.setNext(current.getNext());
                }
                current = null;
                deleted = true;
                break;
            }else{
                prev= current;
                current = current.getNext();
            }
        }
        return deleted;
    }
}
